﻿// Important! Download iText 7 Community for .NET from https://github.com/itext/itext7-dotnet/releases
// and add references to itext.kernel.dll and itext.io.dll to the project

using System;
using System.Globalization;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;

namespace resizePDF
{
    class Program
    {
        static string localDir = @"d:\Languages\Dictionaries\DATA\French\Grand Larousse de la langue française\Pages\";
        static string sourceFile = "Grand Larousse de la langue française. Tome 7.pdf";
        static string destinationFile = "Grand Larousse de la langue française. Tome 7 (A4).pdf";
        static float fixedWidth = 595f;       // A4 width in points
        static float fixedHeight = 842f;      // A4 heigth in points

        static void Main(string[] args)
        {
            string srcPath = System.IO.Path.Combine(localDir, sourceFile);
            string destPath = System.IO.Path.Combine(localDir, destinationFile);
            ScalePdfPagesToFixedSize(srcPath, destPath);
        }

        static void ScalePdfPagesToFixedSize(string sourceFilePath, string destinationFilePath)
        {
            PdfDocument pdfDoc = new PdfDocument(new PdfReader(sourceFilePath), new PdfWriter(destinationFilePath));
            int nPages = pdfDoc.GetNumberOfPages();
            Rectangle crop = new Rectangle(0, 0, fixedWidth, fixedHeight);
            for (int i = 1; i <= nPages; i++)
            {
                PdfPage page = pdfDoc.GetPage(i);
                Rectangle media = page.GetCropBox();
                if (media == null)
                    media = page.GetMediaBox();
                float width = media.GetWidth();
                float height = media.GetHeight();
                float factor = Math.Min(fixedWidth / width, fixedHeight / height);
                float offsetX = (fixedWidth - width * factor) / 2;
                float offsetY = (fixedHeight - height * factor) / 2;
                page.SetMediaBox(crop);
                page.SetCropBox(crop);
                new PdfCanvas(page.NewContentStreamBefore(), new PdfResources(), pdfDoc).WriteLiteral(string.Format(CultureInfo.InvariantCulture, "\nq {0:F} 0 0 {1:F} {2:F} {3:F} cm\nq\n", factor, factor, offsetX, offsetY));
                new PdfCanvas(page.NewContentStreamAfter(), new PdfResources(), pdfDoc).WriteLiteral("\nQ\nQ\n");
            }
            pdfDoc.Close();
        }
    }
}
